/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.sound.midi.Soundbank;
import java.io.FileInputStream;
import java.util.*;
import java.lang.*;

/**
 *
 * @author Luis
 */
public class Arrays {
    static int aux;
    public static void main(String[] args){

        //This is for Exceptions
        /*
        try{
            Integer.parseInt("hello");
        }catch (NumberFormatException e){
            System.out.println("Unable to format");
        }
        */


        //I will create a  list
        List T = new ArrayList();
        Random rand = new Random();

        //I will add elements to the list
        for (int i =0; i<10;i++){
            aux = rand.nextInt(Integer.parseInt("100"));
            T.add(aux);
        }
        int a =100;
        int other_Rand=rand.nextInt(a);

        //Display the list of Numbers
        System.out.println("The elements of the list are:"+"\t" + T + "\n");

        //Remove elements of the list by index
        System.out.println("The element to remove will be:"+"\t" + T.get(6));
        T.remove(6);
        System.out.println("The new list is:"+"\t" + T + "\n");

        System.out.println("The element to remove will be:"+"\t" + T.get(4));
        T.remove(4);
        System.out.println("The new list is:"+"\t" + T + "\n");

        
        //Add elements of the list by index
        System.out.println("we will add new elements to the list:");
        T.add(7,other_Rand);
        System.out.println("The new list is:"+"\t" + T + "\n");

        //Iterate elements from the List
        System.out.println("we will iterate the elements to the list:");
        T.forEach(t->System.out.println(t));
        System.out.println(" "  + "\n");

        //Sort the List
        Collections.sort(T);
        System.out.println("This is the List sorted"  + "\n");
        T.forEach(t->System.out.println(t));
        System.out.println(" "  + "\n");

        /*
        * This is the part where I validate a data enter
        * */

            //Search for elements to the List
            // to insert numbers
            Scanner ins = new Scanner(System.in);
            int num=-1;
            do {
                System.out.println("insert a number between 1 and 100");
                try {
                    num = ins.nextInt();
                    if(num<0 || num>100){
                        System.out.println("You entered an invalid number");
                    }

                } catch (InputMismatchException e)
                {
                    System.out.println("You must enter an integer!");

                    ins.nextInt();
                }
            }while (num<0 ||num>100);
        System.out.println("you entered the number"+"\t" +num);


            /*
            System.out.println("we will search elements to the list:");
            if(T.contains(num)){
                System.out.println("The number is on the list.\n");
            }else{
                System.out.println("This number in NOT on the list.\n");
            }
             */



        /*

        // Check the size of the list
        System.out.println("Size of the List");
        System.out.println(T.size() + "\n");
        
        // Use the collection.suffle
        Collections.shuffle(T);
        System.out.println("Suffle List");
        T.forEach(t->System.out.println(t));
        System.out.println(" " + "\n");
        
        // A reversed List
        Collections.sort(T);
        System.out.println("Actual list");
        T.forEach(t->System.out.println(t));
        System.out.println(" " + "\n");
        System.out.println("Reverse List");
        Collections.reverse(T);
        T.forEach(t->System.out.println(t));

         */
    }
}
