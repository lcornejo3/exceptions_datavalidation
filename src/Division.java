import java.util.InputMismatchException;
import java.util.Scanner;

public class Division {

    public static void doDivision(float a, float b){
        float div = 0;
        if(b==0){
            System.err.println("User insert a zero");
            throw new ArithmeticException();
        }else {
            div = a / b;
            System.out.println("The result is:" + div);
        }
    }

    public static void main(String[] args) {
        do{
            Scanner board = new Scanner(System.in);
            try {
                System.out.println("insert the numerator:" + "\t");
                float num1 = board.nextInt();

                System.out.println("insert the denominator:" + "\t");
                float num2 = board.nextInt();

                doDivision(num1, num2);

            }catch (InputMismatchException ime){
                System.err.println("You need to enter an integer. Try again");
                board.nextLine();
                System.err.println("Enter only numbers.");
            }catch (ArithmeticException ae) {
                //throw new ArithmeticException("User entered a zero");
                System.err.println("You cannot divide by zero. Try again");
                //ae.printStackTrace();
            }finally {
                System.out.println("\nDo you want to continue (y/n):");
                String option = board.next();
                if (!option.trim().equals("y"))
                    break;
            }
        }while (true);
    }
}